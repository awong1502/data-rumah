<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Data;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class DataController extends Controller
{
    public function index(){
        return view('index');
    }

    public function showData(){
        $dataOrang = data::all();

        return view('index', ['data' => $dataOrang]);
    }

    public function servicesShow(){
        return view('services.index');
    }
}