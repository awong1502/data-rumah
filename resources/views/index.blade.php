@extends('layouts.app')

@section('content')
    
  
        <!-- Hero Area Start -->
        <div id="hero-area" class="hero-area-bg">
          <div class="overlay"></div>
          <div class="container">
            <div class="row">
              <div class="col-md-12 col-sm-12 text-center">
                <div class="contents">
                  <h5 class="script-font wow fadeInUp" data-wow-delay="0.2s">Selamat Datang Di</h5>
                  <h2 class="head-title wow fadeInUp" data-wow-delay="0.4s">Rumah Cokor</h2><ul class="social-icon wow fadeInUp" data-wow-delay="0.8s">
                    <li>
                      <a class="facebook" href="#"><i class="icon-social-facebook"></i></a>
                    </li>
                    <li>
                      <a class="twitter" href="#"><i class="icon-social-twitter"></i></a>
                    </li>
                    <li>
                      <a class="instagram" href="#"><i class="icon-social-instagram"></i></a>
                    </li>
                    <li>
                      <a class="linkedin" href="#"><i class="icon-social-linkedin"></i></a>
                    </li>
                    <li>
                      <a class="google" href="#"><i class="icon-social-google"></i></a>
                    </li>
                  </ul>
                  <div class="header-button wow fadeInUp" data-wow-delay="1s">
                    <a href="#" class="btn btn-common">Get a Free Quote</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Hero Area End -->
  
      </header>
      <!-- Header Area wrapper End -->
  
      <!-- About Section Start -->
      <section id="about" class="section-padding">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="img-thumb wow fadeInLeft" data-wow-delay="0.3s">
                <img class="img-fluid" src="{{ asset('img/anjas.jpg') }}" alt="">
              </div>
            </div> 
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="profile-wrapper wow fadeInRight" data-wow-delay="0.3s">
                <h3>Hallo Semua !!</h3>
                <p>Selamat datang di web ini, Maaf karena hanya mengunakan template. Jadi untuk dijadikan web secara global belum bisa, jadi harus beli license dulu makanya tolong bantuannya ya semuanya :)</p>
                <div class="about-profile">
                  <ul class="admin-profile">
                    @foreach ($data as $d)
                        
                    <li><span class="pro-title"> Name </span> <span class="pro-detail">Zulkarnain Mukhtar</span></li>
                    <li><span class="pro-title"> Age </span> <span class="pro-detail">{{ $d->umur }}</span></li>
                    <li><span class="pro-title"> Experience </span> <span class="pro-detail">{{ $d->pengalaman }}</span></li>
                    <li><span class="pro-title"> Country </span> <span class="pro-detail">{{ $d->negara }}</span></li>
                    <li><span class="pro-title"> Location </span> <span class="pro-detail">{{ $d->alamat }}</span></li>
                    <li><span class="pro-title"> e-mail </span> <span class="pro-detail">awong1502@gmail.com</span></li>
                    <li><span class="pro-title"> Phone </span> <span class="pro-detail">{{ $d->nomor }}</span></li>
                    <li><span class="pro-title"> Freelance </span> <span class="pro-detail">Tersedia</span></li>

                    @endforeach
                  </ul>
                </div>
                <a href="#" class="btn btn-common"><i class="icon-paper-clip"></i> Download Resume</a>
                <a href="#" class="btn btn-danger"><i class="icon-speech"></i> Hubungi Saya</a>
              </div>
            </div>   
          </div>
        </div>
      </section>
      <!-- About Section End -->
  
    
      <!-- Footer Section Start -->
      <footer class="footer-area section-padding">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="footer-text text-center wow fadeInDown" data-wow-delay="0.3s">
                <ul class="social-icon">
                  <li>
                    <a class="facebook" href="#"><i class="icon-social-facebook"></i></a>
                  </li>
                  <li>
                    <a class="twitter" href="#"><i class="icon-social-twitter"></i></a>
                  </li>
                  <li>
                    <a class="instagram" href="#"><i class="icon-social-instagram"></i></a>
                  </li>
                  <li>
                    <a class="instagram" href="#"><i class="icon-social-linkedin"></i></a>
                  </li>
                  <li>
                    <a class="instagram" href="#"><i class="icon-social-google"></i></a>
                  </li>
                </ul>
                <p>Copyright © 2020 aWONG All Right Reserved</p>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <!-- Footer Section End -->
  
@endsection